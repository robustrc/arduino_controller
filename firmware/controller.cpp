/*
 * rcremote.ino
 * RC Project
 *
 * Intends to take control message and deliver to DAC
 * which is in turn connected to the real RC remote.
 */

#include <Arduino.h>
#include <ros.h>
#include <std_msgs/String.h>
#include <rc_msgs/Control.h>

#include <MCPDAC-master/MCPDAC.h>
#include <SPI.h>


ros::NodeHandle  nh;

void controlCb(const rc_msgs::Control& msg) {
    uint16_t speed = msg.speed;
    uint16_t steering = msg.steering;

    MCPDAC.setVoltage(CHANNEL_A, steering);
    MCPDAC.setVoltage(CHANNEL_B, speed);

}

ros::Subscriber<rc_msgs::Control> controlSub("control", &controlCb);

void setup()
{
    nh.initNode();
    nh.subscribe(controlSub);


    //CS on pin 8, no LDAC
    MCPDAC.begin(8);

    MCPDAC.setGain(CHANNEL_A, GAIN_HIGH);
    MCPDAC.setGain(CHANNEL_B, GAIN_HIGH);

}

void loop()
{
    nh.spinOnce();
    delay(1);
}
