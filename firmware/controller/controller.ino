
/*
 * rcremote.ino
 * RC Project
 *
 * Intends to take control message and deliver to DAC
 * which is in turn connected to the real RC remote.
 */


#include <Arduino.h>
#include <ros.h>
#include <std_msgs/String.h>
#include <rc_msgs/ControlSignal.h>

#include <MCPDAC.h>
#include <SPI.h>


// MAX RIGHT = 3200 mV
// MAX LEFT = 0 mV
// MAX THROTTLE = 3200 mV
// MAX BRAKE = 0 mV

// CHANNEL_A = POWER
// CHANNEL_B = DELTA

ros::NodeHandle  nh;
bool okToRun = 0;
bool proceed = 0;
int loopRate = 500;
uint16_t power;
uint16_t delta;
uint16_t maxSteering = 25;
uint16_t maxThrottle = 1; // Placeholder value

void controlCb(const rc_msgs::ControlSignal& msg) {
    float powerF = msg.power; // Rename this to throttle or similar (done in rc_msgs)
    float deltaF = msg.delta; // Rename this to steering
    convertControlSignal2Voltage(powerF, deltaF);
    // Guards to avoid burning stuff.
    if (power > 3200)
    {
      power = 3200;
    }
    else if (power < 0) {
      power = 0;
    }
    if (delta < 0)
    {
      delta = 0;
    }
    else if (delta > 3200) {
      delta = 3200;
    }

    MCPDAC.setVoltage(CHANNEL_A, power);
    MCPDAC.setVoltage(CHANNEL_B, delta);

}

void resetController() {
    Serial.println("Please turn off the transmitter, then press enter.");
    while (!proceed) {
      if (Serial.available() > 0) {
        if (Serial.read() == 10) {
          proceed = 1;
        }
      }
    }
    proceed = 0;
    
    MCPDAC.setVoltage(CHANNEL_A, 1600);
    MCPDAC.setVoltage(CHANNEL_B, 1600);
    delay(250);
    
    Serial.println("Please turn all the knobs to their maximum COUNTER-CLOCKWISE direction, then press enter.");
    while (!proceed) {
      if (Serial.available() > 0) {
        if (Serial.read() == 10) {
          proceed = 1;
        }
      }
    }
    proceed = 0;
    
    MCPDAC.setVoltage(CHANNEL_A, 0);
    MCPDAC.setVoltage(CHANNEL_B, 3200);
    delay(250);

    
    Serial.println("Please turn on the transmitter, LED should now be flashing rapidly. Press enter.");
    while (!proceed) {
      if (Serial.available() > 0) {
        if (Serial.read() == 10) {
          proceed = 1;
        }
      }
    }
    proceed = 0;
    
    MCPDAC.setVoltage(CHANNEL_A, 1600);
    MCPDAC.setVoltage(CHANNEL_B, 1600);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_B, 0);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_B, 3200);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_B, 0);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_B, 3200);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_B, 1600);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_A, 3200);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_A, 0);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_A, 3200);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_A, 0);
    delay(250);
    MCPDAC.setVoltage(CHANNEL_A, 1600);
    delay(250);

    Serial.println("Please turn all the knobs to their maximum CLOCKWISE direction, LED should now be solid. Press enter.");
    while (!proceed) {
      if (Serial.available() > 0) {
        if (Serial.read() == 10) {
          proceed = 1;
        }
      }
    }
    proceed = 0;
    Serial.println("Now turn off the transmitter, then press enter.");
    while (!proceed) {
      if (Serial.available() > 0) {
        if (Serial.read() == 10) {
          proceed = 1;
        }
      }
    }
    proceed = 0;
    Serial.println("Turn it back on, then press enter.");
    while (!proceed) {
      if (Serial.available() > 0) {
        if (Serial.read() == 10) {
          proceed = 1;
        }
      }
    }
    proceed = 0;
    Serial.println("You are done, make the adjustments to trim and stuff that you feel are necessary.");
    while (!proceed) {
      if (Serial.available() > 0) {
        if (Serial.read() == 10) {
          proceed = 1;
        }
      }
    }
    proceed = 0;
}


void initController() {
  
    MCPDAC.setVoltage(CHANNEL_A, 1600);
    MCPDAC.setVoltage(CHANNEL_B, 1600);
    delay(1000);
}

void convertControlSignal2Voltage(float throttle, float steering) {
  power = (throttle + maxThrottle) * (3200/(2*maxThrottle)); // Map the signals to Voltages
  delta = (steering + maxSteering) * (3200/(2*maxSteering)); // -------------||-------------
}

void setup()
{
    nh.initNode();
    //CS on pin 6 and 7 (different cars)
    MCPDAC.begin(7);
    
    MCPDAC.setGain(CHANNEL_A, GAIN_HIGH);
    MCPDAC.setGain(CHANNEL_B, GAIN_HIGH);
    
    Serial.println("Initialising controller");
    initController();
    Serial.println("Init done, start the transmitter, then press enter.");
    while (!okToRun) {
      if (Serial.available() > 0) {
        if (Serial.read() == 10) {
          okToRun = 1;
        }
      }  
    }
    okToRun = 0;
    ros::Subscriber<rc_msgs::ControlSignal> controlSub("control", &controlCb);
    nh.subscribe(controlSub);

}

void loop()
{
  
  Serial.println("Press enter to begin the mayhem");
  while (!okToRun) {
    //resetController();
    if (Serial.available() > 0) {
      if (Serial.read() == 10) {
        okToRun = 1;
      }
    }  
  }
    nh.spinOnce();
    delay(1000/loopRate);
}
