cmake_minimum_required(VERSION 2.8.3)

include_directories(${ROS_LIB_DIR})

add_definitions(-DUSB_CON)

generate_arduino_firmware(controller
  SRCS controller.cpp ${ROS_LIB_DIR}/time.cpp MCPDAC-master/MCPDAC.cpp
  BOARD uno
  PORT /dev/ttyACM0
)
