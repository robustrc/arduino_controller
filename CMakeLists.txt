cmake_minimum_required(VERSION 2.8.3)
project(arducontroller)

find_package(catkin REQUIRED COMPONENTS
  rc_msgs
  rosserial_arduino
  rosserial_client
)

catkin_package()

include_directories(
  ${catkin_INCLUDE_DIRS}
)

rosserial_generate_ros_lib(
  PACKAGE rosserial_arduino
  SCRIPT make_libraries.py
)

rosserial_configure_client(
  DIRECTORY firmware
  #TOOLCHAIN_FILE ${ROSSERIAL_ARDUINO_TOOLCHAIN}
  TOOLCHAIN_FILE /opt/ros/lunar/share/rosserial_arduino/arduino-cmake/cmake/ArduinoToolchain.cmake
)

rosserial_add_client_target(firmware controller ALL)
rosserial_add_client_target(firmware controller-upload)
